package refactorizacion;
/*
 * Clase para refactorizar
 * Codificacion UTF-8
 * Paquete: ra3.refactorizacion
 * Se deben comentar todas las refactorizaciones realizadas,
 * mediante comentarios de una linea o de bloque.
 */

/*import java.*; La elimino porque sobra*/
/*Elimino el * ya que solo se usa la clase Scanner*/
import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
/*Cambio el nombre de clase para que sea mas descriptivo*/
public class EjercicioRefactorizacion1 {
	/*Cambio el nombre de la variable constante a mayusculas y la renombro para que sea 
	 * mas descriptiva
	 */
	final static String SALUDO_INICIO_PROGRAMA = "Bienvenido al programa";
	/*Cambio nombre del array para que sea mas descriptivo y ademas es constante sus valores*/
	final static String[] DIAS_SEMANA = {"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
	public static void main(String[] args) {
		/*Declaracion de variables en lineas separadas*/
		/*Cambio el nombre de las variables para que sean mas descriptivas*/
		int condicionA; 
		int condicionB;
		String dniUsuario;
		String nombreUsuario;
		Scanner entradaDatos = new Scanner(System.in);
		
		System.out.println(SALUDO_INICIO_PROGRAMA);
		System.out.println("Introduce tu dni");
		dniUsuario = entradaDatos.nextLine();
		System.out.println("Introduce tu nombre");
		nombreUsuario = entradaDatos.nextLine();
		
		condicionA=7; condicionB=16;
		int numeroC = 25;
		if(condicionA > condicionB || numeroC % 5 != 0 && (numeroC * 3 - 1) > condicionB / numeroC){
			System.out.println("Se cumple la condición");
		}
		
		numeroC = condicionA + (condicionB * numeroC) + (condicionB / condicionA);
		
		
		
		/*Metodo renombrado para que sea mas descriptivo*/
		queDiaDeLaSemanaEs(DIAS_SEMANA);
	}
	/*Metodo renombrado para que sea mas descriptivo*/
	/*Cambio nombre parametro para que sea mas descriptivo*/
	static void queDiaDeLaSemanaEs(String[] nombresDiasSemana){/*Alineado de llaves*/
		for(int i = 0; i < nombresDiasSemana.length; i++){/*Alineado de llaves*/
			System.out.println("El dia de la semana en el que te encuentras [" + 
			(i + 1) + "-7] es el dia: " + nombresDiasSemana[i]);
		}
	}
	
}