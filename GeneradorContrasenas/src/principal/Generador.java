package principal;

import java.util.Scanner;

public class Generador {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);		
		mostrarInfo();			
		switch (obtenerTipoPassword(scanner)) {
		case 1:
			System.out.println(caso1(obtenerLongitudCadena(scanner)));
			break;
		case 2:
			System.out.println(caso2(obtenerLongitudCadena(scanner)));
			break;
		case 3:
			System.out.println(caso3(obtenerLongitudCadena(scanner)));
			break;
		case 4:
			System.out.println(caso4(obtenerLongitudCadena(scanner)));
			break;
		}
		scanner.close();
	}
	
	static int obtenerLongitudCadena(Scanner lector){
		System.out.println("Introduce la longitud de la cadena: ");
		return lector.nextInt();
	}
	
	static int obtenerTipoPassword(Scanner lector){
		System.out.println("Elige tipo de password: ");
		return lector.nextInt();
	}
	
	static String caso1(int longitud){
		String password = "";
		for (int i = 0; i < longitud; i++) {
			password += obtenerLetraAleatoria();
		}
		return password;
	}
	
	static char obtenerLetraAleatoria(){
		return (char) ((Math.random() * 26) + 65);
	}
	
	static String caso2(int longitud){
		String password = "";
		for (int i = 0; i < longitud; i++) {
			password += obtenerNumeroAleatorio(10);
		}		
		return password;
	}
	
	static int obtenerNumeroAleatorio(int max){
		return (int) (Math.random() * max);
	}
	
	static String caso3(int longitud){
		String password = "";
		for (int i = 0; i < longitud; i++) {
			int n = obtenerNumeroAleatorio(2);
			if (n == 1) {
				password += obtenerLetraAleatoria();
			} else {
				password += (char) ((obtenerNumeroAleatorio(2)) + 33);
			}
		}		
		return password;
	}
	
	static String caso4(int longitud){
		String password = "";
		for (int i = 0; i < longitud; i++) {
			int n = obtenerNumeroAleatorio(3);
			if (n == 1) {
				password += obtenerLetraAleatoria();
			} else if (n == 2) {
				char caracter4;
				caracter4 = (char) ((obtenerNumeroAleatorio(15)) + 33);
				password += caracter4;
			} else {
				password += obtenerNumeroAleatorio(10);
			}
		}		
		return password;
	}

	static void mostrarInfo(){
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}
}