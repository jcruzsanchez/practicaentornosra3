package principal;

import java.io.File;
import java.util.Scanner;

public class Principal {
	private final static byte NUM_PALABRAS = 20;
	private final static byte FALLOS = 7;
	private static String[] palabras = new String[NUM_PALABRAS];
	private static String RUTA = "src\\palabras.txt";
	public static void main(String[] args) {
		String palabraSecreta = palabraSecreta();		
		Scanner input = new Scanner(System.in);		

		char[][] caracteresPalabra = new char[2][];
		caracteresPalabra[0] = palabraSecreta.toCharArray();
		caracteresPalabra[1] = new char[caracteresPalabra[0].length];

		String caracteresElegidos = "";
		int fallos;
		boolean acertado = true;
		System.out.println("Acierta la palabra");
		do {

			muestraEstado(caracteresPalabra, caracteresElegidos);
			caracteresElegidos += input.nextLine().toUpperCase();
			fallos = 0;

			boolean encontrado;
			for (int j = 0; j < caracteresElegidos.length(); j++) {
				encontrado = false;
				for (int i = 0; i < caracteresPalabra[0].length; i++) {
					if (caracteresPalabra[0][i] == caracteresElegidos.charAt(j)) {
						caracteresPalabra[1][i] = '1';
						encontrado = true;
					}
				}
				if (!encontrado)
					fallos++;
			}

			muestraPorPantalla(fallos);
			muestraSiHaPerdido(fallos, palabraSecreta);
			
			if (haAcertado(caracteresPalabra)){
				System.out.println("Has Acertado ");
			}
			
		} while (!acertado && fallos < FALLOS);

		input.close();
	}
	
	static boolean haAcertado(char[][] caracteresPalabra){
		boolean acertado = true;
		for (int i = 0; i < caracteresPalabra[1].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				acertado = false;
				break;
			}
		}
		return acertado;
	}
	
	static void muestraEstado(char[][] caracteresPalabra, String caracteresElegidos){
		System.out.println("####################################");

		for (int i = 0; i < caracteresPalabra[0].length; i++) {
			if (caracteresPalabra[1][i] != '1') {
				System.out.print(" -");
			} else {
				System.out.print(" " + caracteresPalabra[0][i]);
			}
		}
		System.out.println();

		System.out.println("Introduce una letra o acierta la palabra");
		System.out.println("Caracteres Elegidos: " + caracteresElegidos);
	}
	
	static String palabraSecreta(){
		File fich = new File(RUTA);
		Scanner inputFichero = null;

		try {
			inputFichero = new Scanner(fich);
			for (int i = 0; i < NUM_PALABRAS; i++) {
				palabras[i] = inputFichero.nextLine();
			}
		} catch (Exception e) {
			System.out.println("Error al abrir fichero: " + e.getMessage());
		} finally {
			if (fich != null && inputFichero != null)
				inputFichero.close();
		}
		return palabras[(int) (Math.random() * NUM_PALABRAS)];
	}
	
	static void muestraSiHaPerdido(int fallos, String palabraSecreta){
		if (fallos >= FALLOS) {
			System.out.println("Has perdido: " + palabraSecreta);
		}
	}
	
	static void muestraPorPantalla(int fallos){
		switch (fallos) {
		case 1:
			System.out.println("     ___");
			break;
		case 2:
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 3:
			System.out.println("  ____ ");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 4:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println("      |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 5:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println("      |");
			System.out.println("     ___");
			break;
		case 6:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println("     ___");
			break;
		case 7:
			System.out.println("  ____ ");
			System.out.println(" |    |");
			System.out.println(" O    |");
			System.out.println(" T    |");
			System.out.println(" A   ___");
			break;
		}
	}

}
